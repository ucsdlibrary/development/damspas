[![pipeline status](https://gitlab.com/ucsdlibrary/development/damspas/badges/master/pipeline.svg)](https://gitlab.com/ucsdlibrary/development/damspas/-/commits/master)
[![coverage report](https://gitlab.com/ucsdlibrary/development/damspas/badges/master/coverage.svg)](https://gitlab.com/ucsdlibrary/development/damspas/-/commits/master)

[UC San Diego Library](https://library.ucsd.edu/ "UC San Diego Library") Digital Collections Public Access System.

A Hydra repository backed by [DAMS Repository](http://github.com/ucsdlib/damsrepo).

# Docker
You can use the provided helper script as below, or invoke `docker-compose`
commands directly against the files in `docker/`. See the [Docker README](docker/README.md) for more information.

``` sh
$ ./bin/dc up
$ ./bin/dc exec web rake db:setup
```
Then navigate to localhost:3000

# Virtual Reading Room

The application has a virtual reading room feature that depends on the Aeon REST
API to facilitate management of requests to ordinarily private materials.

The developer-focused documentation from Notch 8 is provided in [doc/UCSD-Virtual-Reading-Room.pdf](doc/UCSD-Virtual-Reading-Room.pdf)
# Deploying with GitLab ChatOps
This application is setup currently for deployments to be done via GitLab's
chatops feature.

The supported environments to deploy to, via Capistrano, are:
- pontos
- staging
- production

Example: `/gitlab ucsdlibrary/development/damspas run production_deploy`

By default, Capistrano will deploy the specified default branch, which is
`master`. If you would like to specify a different branch or tag, you can pass
that as an argument to the slack command.

Example: `/gitlab ucsdlibrary/development/damspas run production_deploy 1.5.0`

The above command will deploy the `1.5.0` tag to `production`.

## Scheduling A Production Deployment
The application also supports setting up a GitLab [Scheduled
Pipeline][gitlab-schedule] to support production deployments during the
Operations service window (6-8am PST).

An example schedule using [Cron syntax][cron] might be `30 06 * * 2` which would deploy
the application at 06:30AM on Monday.

Make sure you set the `Cron timezone` in the schedule for `Pacific Time (US and
Canada)`

After the deploy, make sure you de-active or delete the Schedule.
Otherwise the application may accidentally deploy again the following week.

## Rollbacks with GitLab ChatOps
If a deployment gets far enough that it cannot be automatically rolled back, you
can make calls to rollback to the previous release. Internally, this uses the
Capistrano `deploy:rollback` task.

Example: `/gitlab ucsdlibrary/development/damspas run production_rollback`

[cron]:https://en.wikipedia.org/wiki/Cron
[gitlab-schedule]:https://docs.gitlab.com/ee/ci/pipelines/schedules.html
