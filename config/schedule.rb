env 'RAILS_RELATIVE_URL_ROOT', '/dc'

# Note: The AEON API currently has a problem processing concurrent requests
#   So we're offsetting the cron times to try avoiding that for now
#   This should be able to be deleted in the future
case @environment
when 'production'
  every '*/2 * * * *' do
    rake "aeon_requests:process_new"
  end
  every 1.day do
    rake "aeon_requests:revoke_old"
  end
  every 1.day, at: '2:00 am' do
    rake "sitemap:refresh:no_ping" # pinging Google is deprecated
  end
when 'staging'
  every '*/2 * * * *' do
    rake "aeon_requests:process_new"
  end
  every 1.day, at: '1:00 am' do
    rake "aeon_requests:revoke_old"
  end
when 'qa'
  every '50 * * * *' do
    rake "aeon_requests:process_new"
  end
  every 1.day, at: '1:00 am' do
    rake "aeon_requests:revoke_old"
  end
when 'ops'
  every '50 * * * *' do
    rake "aeon_requests:process_new"
  end
  every 1.day, at: '1:00 am' do
    rake "aeon_requests:revoke_old"
  end
end
