set :stage, :staging
set :branch, (ENV['BRANCH'] || fetch(:branch, 'trunk'))
server 'lib-hydrahead-staging.ucsd.edu', user: 'conan', roles: %w{web app db}
set :rails_env, "staging"
