set :stage, :ops
set :branch, (ENV['BRANCH'] || fetch(:branch, 'trunk'))
server 'lib-hydrahead-ops.ucsd.edu', user: 'conan', roles: %w{web app db}
set :rails_env, "ops"
