set :stage, :qa
set :branch, (ENV['BRANCH'] || fetch(:branch, 'trunk'))
server 'lib-hydrahead-qa.ucsd.edu', user: 'conan', roles: %w{web app db}
set :rails_env, "qa"
