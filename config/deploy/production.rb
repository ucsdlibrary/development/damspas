set :stage, :production
set :branch, (ENV['BRANCH'] || fetch(:branch, 'trunk'))
server 'lib-hydrahead-prod.ucsd.edu', user: 'conan', roles: %w{web app db}
set :rails_env, "production"
