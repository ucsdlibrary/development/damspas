set :stage, :pontos
set :branch, (ENV['BRANCH'] || fetch(:branch, 'trunk'))
server 'pontos.ucsd.edu', user: 'conan', roles: %w{app db}
set :rails_env, "pontos"
