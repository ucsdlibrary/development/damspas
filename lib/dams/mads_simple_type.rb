module Dams
  module MadsSimpleType
    # = MADS SimpleType, extended by MADS Topic, Geographic, etc.
    extend ActiveSupport::Concern
    include ModelHelper
    included do
      rdf_subject { |ds|
        if ds.pid.nil?
          RDF::URI.new
        else
          RDF::URI.new(Rails.configuration.id_namespace + ds.pid)
        end
      }
      map_predicates do |map|
        map.name(:in => MADS, :to => 'authoritativeLabel')
        map.externalAuthority(:in => MADS, :to => 'hasExactExternalAuthority')
        map.hasVariant(:in => MADS, :class_name => 'MadsVariant')
        map.hiddenVariant(:in => MADS, :to => 'hasHiddenVariant')
        map.scheme(:in => MADS, :to => 'isMemberOfMADSScheme', :class_name => 'MadsSchemeInternal')
      end

      accepts_nested_attributes_for :hasVariant

      def solr_base (solr_doc={})
        Solrizer.insert_field(solr_doc, 'name', name)
        if scheme.first
          Solrizer.insert_field(solr_doc, 'scheme', scheme.first.rdf_subject.to_s)
          Solrizer.insert_field(solr_doc, 'scheme_name', scheme.first.name.first)
          Solrizer.insert_field(solr_doc, 'scheme_code', scheme.first.code.first)
        end
        Solrizer.insert_field(solr_doc, "externalAuthority", externalAuthority.first.to_s)

        insertSolrFields solr_doc, 'variant', variant
        insertSolrFields solr_doc, 'hidden_variant', hiddenVariant

        solr_doc
      end

      def variant
        variants(hasVariant)
      end

      def variant=(val)
        set_variant(hasVariant, val)
      end

      def variants(type)
        vars = Array.new
        type.each do |var|
          vars << var.variantLabel.first
        end
        vars
      end

      def set_variant(type, val)
        if val.class == Array
          val = val.first
        end
        if(!val.nil? && val.length > 0)
          type.build if type[0] == nil
          type[0].variantLabel = val
        end     
      end
    end

  end
end
