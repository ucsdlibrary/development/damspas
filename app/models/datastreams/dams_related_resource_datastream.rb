class DamsRelatedResourceDatastream < ActiveFedora::RdfxmlRDFDatastream
  include Dams::DamsHelper
  include Dams::DamsRelatedResource
end
