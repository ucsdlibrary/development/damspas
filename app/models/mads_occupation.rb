class MadsOccupation < ActiveFedora::Base
  has_metadata 'damsMetadata', :type => MadsOccupationDatastream 
  has_attributes :name, :scheme, :elementList, :externalAuthority, :variant, :hasVariant_attributes, :hiddenVariant, :occupationElement_attributes, :occupationElement, :scheme_attributes, datastream: :damsMetadata, multiple: true
  
end
