class DamsCommonName < ActiveFedora::Base
  has_metadata 'damsMetadata', :type => DamsCommonNameDatastream 
  has_attributes :name, :scheme, :elementList, :externalAuthority, :variant, :hasVariant_attributes, :hiddenVariant, :commonNameElement_attributes, :commonNameElement, :scheme_attributes, datastream: :damsMetadata, multiple: true
  
end
