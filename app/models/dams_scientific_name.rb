class DamsScientificName < ActiveFedora::Base
  has_metadata 'damsMetadata', :type => DamsScientificNameDatastream 
  has_attributes :name, :scheme, :elementList, :externalAuthority, :variant, :hasVariant_attributes, :hiddenVariant, :scientificNameElement_attributes, :scientificNameElement, :scheme_attributes, datastream: :damsMetadata, multiple: true
  
end
