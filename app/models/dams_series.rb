class DamsSeries < ActiveFedora::Base
  has_metadata 'damsMetadata', :type => DamsSeriesDatastream
  has_attributes :name, :scheme, :elementList, :externalAuthority, :variant, :hasVariant_attributes, :hiddenVariant, :seriesElement_attributes, :seriesElement, :scheme_attributes, datastream: :damsMetadata, multiple: true
end
