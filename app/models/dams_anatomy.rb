class DamsAnatomy < ActiveFedora::Base
  has_metadata 'damsMetadata', :type => DamsAnatomyDatastream
  has_attributes :name, :scheme, :elementList, :externalAuthority, :variant, :hasVariant_attributes, :hiddenVariant, :anatomyElement_attributes, :anatomyElement, :scheme_attributes, datastream: :damsMetadata, multiple: true
end
