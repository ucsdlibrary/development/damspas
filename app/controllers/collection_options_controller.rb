class CollectionOptionsController < ApplicationController

  #
  #GET /collection_options/default_sort
  #
  def default_sort
    return unless params[:collection_pid]
    
    @collection_option = CollectionOption.find_by(collection_pid: params[:collection_pid])
    
    if @collection_option
      @collection_option.collection_default_sort = params[:collection_default_sort]
      @collection_option.collection_sort_label = params[:label]
      @collection_option.save
      flash[:alert] = "Default Sort has been set to #{params[:label]}."
    else    
      @collection_option = CollectionOption.new(collection_pid: params[:collection_pid], collection_default_sort: params[:collection_default_sort],
                                                collection_sort_label: params[:label])
	  if @collection_option.save
	    flash[:alert] = "Default Sort has been set to #{params[:label]}."  	      
	  else
	    flash[:alert] = "Set Default Sort failed."    
	  end
    end
    redirect_to dams_collection_path(@collection_option.collection_pid)
  end
end
