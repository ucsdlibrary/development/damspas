class CreateCollectionOptions < ActiveRecord::Migration
  def change
    create_table :collection_options do |t|
      t.string :collection_pid
      t.string :collection_default_sort
      t.string :collection_sort_label

      t.timestamps
    end
  end
end
