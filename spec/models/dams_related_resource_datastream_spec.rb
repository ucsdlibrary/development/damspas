require 'spec_helper'

describe DamsRelatedResourceDatastream do
  describe "a vocabulary model" do
    describe "instance populated in-memory" do
      subject { DamsRelatedResourceDatastream.new(double('inner object', :pid=>'xx10101010', :new_record? => true), 'damsMetadata') }

      it "should have a subject" do
        expect(subject.rdf_subject.to_s).to eq("#{Rails.configuration.id_namespace}xx10101010")
      end

      it "should have a type" do
        subject.type = "online finding aid"
        expect(subject.type).to eq(["online finding aid"])
      end

      it "should have a description" do
        subject.description = 'External Related Resource'
        expect(subject.description).to eq(["External Related Resource"])
      end

      it "should have a uri" do
        subject.uri = 'https://rexample.com/any/uri'
        expect(subject.uri).to eq(['https://rexample.com/any/uri'])
      end

      it "should have a displayLabel" do
        subject.displayLabel = 'Online finding aid'
        expect(subject.displayLabel).to eq(['Online finding aid'])
      end

      it "should have an order" do
        subject.order = 5
        expect(subject.order).to eq(['5'])
      end

      it "should have a prefix " do
        subject.prefix = 'Prefix'
        expect(subject.prefix).to eq(['Prefix'])
      end
    end

    describe "an instance loaded from fixture xml" do
      subject do
        subject = DamsRelatedResourceDatastream.new(double('inner object', :pid=>'xx10101010', :new_record? =>true), 'damsMetadata')
        subject.content = File.new('spec/fixtures/damsRelatedResource.rdf.xml').read
        subject
      end

      it "should have a subject" do
        expect(subject.rdf_subject.to_s).to eq("#{Rails.configuration.id_namespace}xx10101010")
      end

      it "should have a type" do
        expect(subject.type).to eq(["online finding aid"])
      end

      it "should have a description" do
        expect(subject.description).to eq(["External Related Resource"])
      end

      it "should have a uri" do
        expect(subject.uri).to eq(['https://rexample.com/any/uri'])
      end

      it "should have a displayLabel" do
        expect(subject.displayLabel).to eq(['Online finding aid'])
      end

      it "should have an order" do
        expect(subject.order).to eq(['5'])
      end

      it "should have a prefix " do
        expect(subject.prefix).to eq(['Prefix'])
      end
    end
  end

  describe "to_solr" do
    subject do
      subject = DamsCartographicsDatastream.new(double('inner object', :pid=>'xx10101010', :new_record? =>true), 'damsMetadata')
      subject.content = File.new('spec/fixtures/damsRelatedResource.rdf.xml').read
      subject
    end

    it "should have no errors" do
      expect{ subject.to_solr }.not_to raise_error
    end
  end
end
