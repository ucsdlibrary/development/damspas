FROM ruby:2.3.7-alpine as production

# Maintainer
MAINTAINER "Matt Critchlow <mcritchlow@ucsd.edu">

RUN apk add --no-cache \
  build-base \
  busybox \
  ca-certificates \
  curl \
  git \
  gnupg1 \
  gpgme \
  less \
  libffi-dev \
  libxml2-dev \
  libxslt-dev \
  linux-headers \
  libsodium-dev \
  nodejs \
  nodejs-npm \
  openssh-client \
  postgresql-dev \
  tzdata \
  rsync \
  wget

# Install phantomjs
RUN wget -qO- "http://nyx.ucsd.edu/damspas/dockerized-phantomjs.tar.gz" | tar xz -C / \
  && npm config set user 0 \
  && npm install -g phantomjs-prebuilt bower

# Trick to copy in Gemfile before other files.
# This way bundle install step only runs again if THOSE files change
COPY Gemfile* /usr/src/app/
WORKDIR /usr/src/app
RUN bundle install
COPY . /usr/src/app/

RUN bundle exec rake bower:install CI=true

CMD ["rails", "s", "-b", "0.0.0.0"]
